#!/usr/bin/env python3

# Script that will format markdown nicely

import argparse
import re
import sys

NEW_LINE = '\n'
LINE_BREAKING_CHARACTERS = [
    ' ',
    ';',
    '\'',
    '*',
    '.',
    '`',
    ':',
    ']',
    '}',
    ')',
    '[',
    '{',
    '(',
    '"',
    '!',
    '?',
]


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('file',
                        help='File to format')
    parser.add_argument('-o',
                        '--output',
                        help='Output to file')
    parser.add_argument('-v',
                        '--verbose',
                        action='store_true',
                        help='Print markdown to terminal')
    parser.add_argument('-b',
                        '--code-blocks',
                        action='store_true',
                        help='Format code blocks')
    parser.add_argument('-c',
                        '--column-width',
                        type=int,
                        default=80,
                        help='Maximum column')
    return parser


def get_args():
    parser = get_parser()
    args = parser.parse_args()
    return args


def read_file(file_name):
    """
    Read a file
    :param file_name: path to file
    :return: string
    """
    with open(file_name, 'r') as fh:
        file_contents = fh.read()
    return file_contents


def write_file(file_name, file_contents):
    """
    Write string to a file
    :param file_name: path to file
    :param file_contents: string
    :return: None
    """
    with open(file_name, 'w') as fh:
        fh.write(file_contents)


def get_leading_whitespace(file_contents, start, end):
    """
    Calculate the leading whitespace needed in the event of inserting a line-feed
    :param file_contents: list of characters
    :param start: starting index in file_contents
    :param end: ending index in file_contents
    :return: list of spaces or empty string
    """
    string = ''.join(file_contents[start:end])
    ordered_list = re.compile(r'^ *\d+. ')
    other = re.compile(r'^ *[>*+\-] ')
    m = ordered_list.match(string) or other.match(string)
    if m:
        count = m.end()
        return list(count * ' ')

    for i, char in enumerate(file_contents[start:]):
        if char != ' ':
            return list(i * ' ')

    return ''


def is_table(file_contents, start, end):
    """
    Determine if line is a markdown table
    :param file_contents: list of characters
    :param start: starting index in file_contents
    :param end: ending index in file_contents
    :return: boolean
    """
    string = ''.join(file_contents[start:end])
    table = re.compile(r'^\|.*\|')
    return True if table.match(string) else False


def contains_link(file_contents, start, end):
    """
    Determine if text contains a markdown link
    :param file_contents: list of characters
    :param start: starting index in file_contents
    :param end: ending index in file_contents
    :return: boolean
    """
    string = ''.join(file_contents[start:end])
    link = re.compile(r'\[.*\]\(.*\)')
    return True if link.match(string) else False


def contains_image(file_contents, start, end):
    """
    Determine if text contains a markdown link
    :param file_contents: list of characters
    :param start: starting index in file_contents
    :param end: ending index in file_contents
    :return: boolean
    """
    string = ''.join(file_contents[start:end])
    image = re.compile(r'!\[.*\]\(.*\)')
    return True if image.match(string) else False


def skip_any_code_block(file_contents, start):
    """
    Jump past any markdown code-blocks
    :param file_contents: list of characters
    :param start: starting index in file_contents
    :return: index just past code-block if it exists
    """
    end = len(file_contents)
    string = ''.join(file_contents[start:end])
    ret = start
    tag = '```'
    tag_len = len(tag)
    if string.startswith(tag):
        indx = string.index(tag, tag_len)
        ret = start + indx + tag_len
    return ret


def next_line_breaking_char(file_contents, pos_1, pos_2, curr_score, goal):
    """
    Walk backwards to find place to insert line-feed
    :param file_contents: list of characters
    :param pos_1: index one
    :param pos_2: index two
    :param curr_score: how far we are from the goal
    :param goal: desired length of line
    :return: (new position, character to add line-feed before)
    """
    start = max(pos_1, pos_2)
    end = min(pos_1, pos_2)
    for i in range(start, end, -1):
        if file_contents[i] in LINE_BREAKING_CHARACTERS:
            if curr_score <= goal:
                return i, file_contents[i]
        curr_score -= 1
    return -1, None


def main():
    args = get_args()
    file_contents = read_file(args.file)

    code_blocks_tag = '```'
    num_code_blocks = file_contents.count(code_blocks_tag)
    if num_code_blocks % 2 != 0:
        print(f'[!] {args.file} contains an odd number of ``` tags. Cannot format.')
        return 1

    goal = args.column_width
    pos = 0
    file_contents = list(file_contents)
    while pos < len(file_contents):
        if not args.code_blocks:
            pos = skip_any_code_block(file_contents, pos)

        try:
            next_lf = file_contents.index(NEW_LINE, pos)
        except ValueError:
            next_lf = len(file_contents) - 1

        if is_table(file_contents, pos, next_lf):
            jump = next_lf + 1
        else:
            white_space = get_leading_whitespace(file_contents, pos, next_lf)
            if (next_lf - pos) >= goal:
                score = next_lf - pos
                indx, char = next_line_breaking_char(file_contents,
                                                     pos,
                                                     next_lf,
                                                     score,
                                                     goal)
                if indx > 0:
                    seam_1 = indx
                    seam_2 = indx
                    if char == ' ':  # get rid trailing space
                        seam_2 = indx + 1
                        pos -= 1
                    part_one = file_contents[:seam_1]
                    part_two = [NEW_LINE] + white_space + file_contents[seam_2:]
                    jump = seam_1 + len(NEW_LINE)
                    file_contents = part_one + part_two
                else:
                    jump = next_lf + 1
            else:
                jump = next_lf + 1

        pos = jump

    new_file_contents = ''.join(file_contents)

    if args.verbose:
        print(new_file_contents)

    if args.output:
        write_file(args.output, new_file_contents)

    return 0


if __name__ == '__main__':
    sys.exit(main())
